/*
	Frequency setting
*/
/*
	We have to calculate the frequency as followin formula
	
	DTMF has two set frequency, we have to decide Row & Column
	with each digit('0','1','2'...)
*/

#define C1 0x94U /* 1209Hz, 1213Hz */
#define C2 0x9EU /* 1336Hz, 1337Hz */
#define C3 0xA7U /* 1477Hz, 1472Hz */
#define C4 0xB0U /* 1633Hz, 1638Hz */

#define R1 0x44U /*  697Hz,  697Hz */
#define R2 0x56U /*  770Hz,  770Hz */
#define R3 0x66U /*  852Hz,  851Hz */
#define R4 0x75U /*  941Hz,  942Hz */ 


/*
	CCITT No.5
*/

#define F1 0x45U	/*  700Hz,  700Hz */
#define F2 0x6EU	/*  900Hz,  897Hz */
#define F3 0x89U	/* 1100Hz, 1101Hz */
#define F4 0x9BU	/* 1300Hz, 1297Hz */
#define F5 0xA9U	/* 1500Hz, 1506Hz */
#define F6 0xB3U	/* 1700Hz, 1702Hz */
#define F7 0xC5U	/* 2200Hz, 2186Hz */
#define F8 0xC9U	/* 2400Hz, 2383Hz */
#define F9 0xCEU	/* 2600Hz, 2621Hz */

