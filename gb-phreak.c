/* ---------------------------------------- */
/*             GB-Phreak Ver1.0             */
/*                  by                      */
/*              Osamu Ohashi                */
/* ---------------------------------------- */

#include <gb.h>
#include <stdlib.h>
#include <stdio.h>

#include "freq.h"

/* BG data */
#include "frame_lcd.c"	/* Back ground pattern */
#include "break_btn.c"	/* button image when broken */
#include "press_btn.c"	/* button image when pressed */

#include "7seg_lcd.c"	/* LCD characters */

/* Sprite data */
#include "key_num.c"	/* Sprite pattern for each key pad */
#include "cursor.c"		/* cursor pattern */

/* 
	usage of BG data
	file name	number of BG	type of matrix	amount
	-------------------------------------------------
	frame_lcd.c  9				8 x  8			 9
	break_btn.c	 9				8 x  8			 9
	press_btn.c  9				8 x  8			 9
	dtmf_lcd.c  31				8 x 16			62
	------------------------------------------------
	total										89


	usage of OBJ data
	file name	number of obj	type of matrix	amount
	--------------------------------------------------
	key_num.c	36				 8 x  8			36
	cursor.c	 1				16 x 16			 4
	--------------------------------------------------
	total										40(Full)
*/


/* display */
#define TITLE "    PHREAK-B0Y    "

/* Background tile */
#define OFFSET 			27

/* Cursor obj start */
/* This number is depned on number of OBJ(SPRITE) */
#define CURSOR_OBJ		36
#define CURSOR_SIZE		 4

#define KEY_STEP 		16	/* Key matrix size as 16 x 16	*/
#define KEY_OFFSET_X	28
#define KEY_OFFSET_Y	44
#define START_CURSOR_X	12	/* CURSOR position	*/
#define START_CURSOR_Y	64
#define UNIT_SIZE_X		 9
#define UNIT_SIZE_Y		 5


#define LCD_X 			1	/* start position of X		*/
#define LCD_Y			2	/* start position of Y		*/
#define LCD_WIDTH		18	/* Horizontal size of LCD	*/
#define LCD_HIGHT		2	/* Vertical Size of LCD		*/


#define ON	1
#define OFF	0

/* DTMF */
#define DTMF_ON		50UL	/* Tone on time		*/
#define DTMF_OFF	50UL	/* Tone off time	*/

/* CCITT 5 */
#define SOUND_ON	60UL	/* Tone on time		*/

#define NICKLE		0		/*  5 cent coin in US */
#define DIME		1		/* 10 cent coin in US */
#define QUARTER		2		/* 25 cent coin in US */

#define NICKLE_TIME		66UL
#define DIME_TIME		66UL
#define QUARTER_TIME	33UL

#define MAX_DTMF	40		/* Maximum length of DTMF strings	*/
char str[MAX_DTMF];

/* Display MODE */
#define DISP_DTMF	0
#define DISP_CCITT	1
#define DTMF	"     DTMF M0DE    "
#define CCITT	"    CC1TT5 M0DE   "

/* Configuration */
#define CON_MSG1	"   C0NFIG M0DE "
#define CON_MSG2	"   0N TIME     "
#define CON_MSG3	"   0FF TIME    "
#define CON_MSG4	"   REPEAT N0   "
#define CON_MSG5	"   PAUSE TIME  "
#define CON_MSG6	"   C0NFIG END  "

#define NORMAL	0
#define CONFIG	1
#define CONFIG_LIMIT	8
#define CONFIG_PARM		3

UWORD on_time, off_time;
UWORD rpt_time, pause_time;

UBYTE disp_mode;
UBYTE config_st;	/* Configuration status such as "ON TIME", "OFF TIME".. */
UBYTE mode_excute;	/* Display The status of this program */
char on_str[CONFIG_PARM], off_str[CONFIG_PARM];
char rpt_str[CONFIG_PARM], pause_str[CONFIG_PARM];

unsigned char row_dtmf[4] = {R1,R2,R3,R4};	/* DTMF frequency strage of Row */	
unsigned char col_dtmf[4] = {C1,C2,C3,C4};	/* DTMF frequency strage of Col */

/* It is possible to set up initial screen by each BG data. */
unsigned char dtmf_tile[] = {
	 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,

	 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2,
	 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5,
	 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5,
	 6, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 8,
	 
	  9,10,10,11, 9,11, 9,11, 9,11, 9,11, 9,10,10,10,10,10,10,11,
	 15,16,16,17,15,17,15,17,15,17,15,17,15,16,16,16,16,16,16,17,

	  9,10,10,11, 9,11, 9,11, 9,11, 9,11, 9,10,10,10,10,10,10,11,
	 15,16,16,17,15,17,15,17,15,17,15,17,15,16,16,16,16,16,16,17,

	  9,10,10,11, 9,11, 9,11, 9,11, 9,11, 9,10,10,10,10,10,10,11,
	 15,16,16,17,15,17,15,17,15,17,15,17,15,16,16,16,16,16,16,17,

	  9,10,10,11, 9,11, 9,11, 9,11, 9,11, 9,10,10,11, 9,10,10,11,
	 15,16,16,17,15,17,15,17,15,17,15,17,15,16,16,17,15,16,16,17,

	  9,10,10,11, 9,10,10,11, 9,11, 9,11, 9,10,10,11, 9,10,10,11,
	 15,16,16,17,15,16,16,17,15,17,15,17,15,16,16,17,15,16,16,17,

	  9,10,10,11, 9,11, 9,11, 9,11, 9,11, 9,10,10,11, 9,10,10,11,
	 15,16,16,17,15,17,15,17,15,17,15,17,15,16,16,17,15,16,16,17,

	 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4
};

/*
	Button image 
	Hint:
	We are using 3 types of buttons.
	Normal size as 1 x 1(tiles are used in 2 x 2)
	Func.
	Special
*/
unsigned char break_tile_1x1[] = {
	 9,11,
	15,17
};

unsigned char break_tile_2x1[] = {
	 9,10,10,11,
	15,16,16,17
};

unsigned char break_tile_4x1[] = {
	 9,10,10,10,10,10,10,11,
	15,16,16,16,16,16,16,17
};

unsigned char press_tile_1x1[] = {
	18,20,
	24,26
};


unsigned char press_tile_2x1[] = {
	18,19,19,20,
	24,25,25,26
};

unsigned char press_tile_4x1[] = {
	18,19,19,19,19,19,19,20,
	24,25,25,25,25,25,25,26
};

/*
	LCD image at initial & AC
*/
unsigned char init_disp[] = {
	59,59,59,59,59,59,59,59,59,59,59,59,59,59,59,59,59,59,
	60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60
};


/*
	Key usage of phreak-boy
	Hint:
	z
*/
char	pad[6][10] = {		/* DTMF Pad assign */
	{'z','z','1','2','3','A','a','a','a','a'},
	{'y','y','4','5','6','B','b','b','b','b'},
	{'x','x','7','8','9','C','c','c','c','c'},
	{'w','w','*','0','#','D','d','d','g','g'},
	{'v','v','s','s','-','%','e','e','h','h'},
	{'u','u',',','?','+','m','f','f','i','i'}
};

unsigned char disp_tile[MAX_DTMF];

/*
	Initialize for sound registers
	ch1, ch2 are used for this routine.
*/
void init_dial()
{

	NR52_REG = 0x83U;
	NR51_REG = 0x00U;
	NR50_REG = 0x77U;

	NR24_REG = 0x87U;
	NR22_REG = 0xffU;
	NR21_REG = 0xbfU;

	NR14_REG = 0x87U;
	NR12_REG = 0xffU;
	NR11_REG = 0xbfU;
	NR10_REG = 0x04U;
}

/* sound engine for DTMF */
void dialtone(UWORD dtmf_on, UWORD dtmf_off, char str[MAX_DTMF])
{
	UBYTE i, j;

	for(j = 0;j < rpt_time;j++){
		i = 0;
		while(str[i]){
			switch(str[i]){
		    	case '1':
					NR13_REG = R1;
					NR23_REG = C1; 
				break;
				case '2':
					NR13_REG = R1;
					NR23_REG = C2;
				break;
				case '3':
					NR13_REG = R1;
					NR23_REG = C3;	
				break;
				case 'A':
				case 'a':
					NR13_REG = R1;
					NR23_REG = C4;  
				break;
				case '4':
					NR13_REG = R2;
					NR23_REG = C1;	
				break;
				case '5':
					NR13_REG = R2;
					NR23_REG = C2;	
				break;
				case '6':
					NR13_REG = R2;
					NR23_REG = C3;	
				break;
				case 'B':
				case 'b':
					NR13_REG = R2;
					NR23_REG = C4;	
				break;
				case '7':
					NR13_REG = R3;
					NR23_REG = C1;	
				break;
				case '8':
					NR13_REG = R3;
					NR23_REG = C2;	
				break;
				case '9':
					NR13_REG = R3;
					NR23_REG = C3;	
				break;
				case 'C':
				case 'c':
					NR13_REG = R3;
					NR23_REG = C4;	
				break;
				case '*':
					NR13_REG = R4;
					NR23_REG = C1;	
				break;
				case '0':
					NR13_REG = R4;
					NR23_REG = C2;	
				break;
				case '#':
					NR13_REG = R4;
					NR23_REG = C3;	
				break;
				case 'D':
				case 'd':
					NR13_REG = R4;
					NR23_REG = C4;	
				break;
				case ',':
					if(dtmf_on)		/* To avoid mistake */
						delay(dtmf_on);	/* keep on */
					if(dtmf_off)
						delay(dtmf_off);	/* keep off */

				default:
					NR51_REG = 0x00U;	/* sound off */
					goto skip;

			}
			NR24_REG = 0x87U;	/* ch2 tips */
			NR51_REG = 0x33U;	/* sound on */
			if(dtmf_on)
				delay(dtmf_on);	/* keep on */

			NR51_REG = 0x00U;	/* sound off */
			if(dtmf_off)
				delay(dtmf_off);/* keep off */

		skip:
			i++;
		}
		if(pause_time)
			delay(pause_time);
	}
}

void ccitttone(UWORD dtmf_on, UWORD dtmf_off, char str[MAX_DTMF])
{
	UBYTE i, j;

	for(j = 0;j < rpt_time;j++){
		i = 0;
		while(str[i]){
			switch(str[i]){
		    	case '1':
					NR13_REG = F1;
					NR23_REG = F2;
				break;
				case '2':
					NR13_REG = F1;
					NR23_REG = F3;
				break;
				case '3':
					NR13_REG = F2;
					NR23_REG = F3;
				break;
				case '4':
					NR13_REG = F1;
					NR23_REG = F4;
				break;
				case '5':
					NR13_REG = F2;
					NR23_REG = F4;
				break;
				case '6':
					NR13_REG = F3;
					NR23_REG = F4;
				break;
				case '7':
					NR13_REG = F1;
					NR23_REG = F5;
				break;
				case '8':
					NR13_REG = F2;
					NR23_REG = F5;
				break;
				case '9':
					NR13_REG = F3;
					NR23_REG = F5;
				break;
				case '0':
					NR13_REG = F4;
					NR23_REG = F5;
				break;
				case ',':
					if(dtmf_on)
						delay(dtmf_on);	/* keep on */
					if(dtmf_off)
						delay(dtmf_off);/* keep off */

				default:
					NR51_REG = 0x00U;	/* sound off */
					goto skip;

			}
			NR24_REG = 0x87U;	/* ch2 tips */
			NR51_REG = 0x33U;	/* sound on */
			if(dtmf_on)
				delay(dtmf_on);	/* keep on */

			NR51_REG = 0x00U;	/* sound off */
			if(dtmf_off)
				delay(dtmf_off);/* keep off */

		skip:
			i++;
		}
		if(pause_time)
			delay(pause_time);
	}
}

void start_sound(UBYTE f1, UBYTE f2)
{
	NR13_REG = f1;
	NR23_REG = f2;
	NR24_REG = 0x87U;

	/* sound output on */
	NR51_REG = 0x33U;
}

void pause_sound(UBYTE on_duration, UBYTE off_duration)
{
	delay(on_duration);
	/* sound output off */
	NR51_REG = 0x00U;
	delay(off_duration);

}


void coin_sound(UBYTE coin)
{
	int i;

	switch(coin){
		case NICKLE:
			start_sound(F7, F6);
			pause_sound(NICKLE_TIME, NICKLE_TIME);
		break;
		case DIME:
			for(i = 0;i < 2;i++){
				start_sound(F7, F6);
				pause_sound(DIME_TIME, DIME_TIME);
			}
		break;
		case QUARTER:
			for(i = 0;i < 5;i++){
				start_sound(F7, F6);
				pause_sound(QUARTER_TIME, QUARTER_TIME);
			}
		break;
	}
}


/* Display looks like 7-SEGMENT LED */
void disp_lcd(UBYTE len, char str[MAX_DTMF])
{
	UBYTE i,j;

	j = len;

	i=0;
	while(str[i]){
		if(str[i] >= '0'||'9' <= str[i]){
			disp_tile[i] = OFFSET + (str[i] - '0') * 2;
			disp_tile[i+j] = OFFSET + (str[i] - '0') * 2 + 1;
		}
		switch(str[i]){
			case 'A':
				disp_tile[i] = OFFSET + 10 * 2;
				disp_tile[i+j] = OFFSET + 10 * 2 + 1;
			break;
			case 'B':
				disp_tile[i] = OFFSET + 11 * 2;
				disp_tile[i+j] = OFFSET + 11 * 2 + 1;
			break;
			case 'C':
				disp_tile[i] = OFFSET + 12 * 2;
				disp_tile[i+j] = OFFSET + 12 * 2 + 1;
			break;
			case 'D':
				disp_tile[i] = OFFSET + 13 * 2;
				disp_tile[i+j] = OFFSET + 13 * 2 + 1;
			break;
			case '#':
				disp_tile[i] = OFFSET + 14 * 2;
				disp_tile[i+j] = OFFSET + 14 * 2 + 1;
			break;
			case '*':
				disp_tile[i] = OFFSET + 15 * 2;
				disp_tile[i+j] = OFFSET + 15 * 2 + 1;
			break;
			case ' ':
				disp_tile[i] = OFFSET + 16 * 2;
				disp_tile[i+j] = OFFSET + 16 * 2 + 1;
			break;
			case 'Y':
				disp_tile[i] = OFFSET + 17 * 2;
				disp_tile[i+j] = OFFSET + 17 * 2 + 1;
			break;
			case 'M':
				disp_tile[i] = OFFSET + 18 * 2;
				disp_tile[i+j] = OFFSET + 18 * 2 + 1;
			break;
			case 'U':
				disp_tile[i] = OFFSET + 19 * 2;
				disp_tile[i+j] = OFFSET + 19 * 2 + 1;
			break;
			case 'G':
				disp_tile[i] = OFFSET + 20 * 2;
				disp_tile[i+j] = OFFSET + 20 * 2 + 1;
			break;
			case '-':
				disp_tile[i] = OFFSET + 21 * 2;
				disp_tile[i+j] = OFFSET + 21 * 2 + 1;
			break;
			case 'T':
				disp_tile[i] = OFFSET + 22 * 2;
				disp_tile[i+j] = OFFSET + 22 * 2 + 1;
			break;
			case ',':
				disp_tile[i] = OFFSET + 23 * 2;
				disp_tile[i+j] = OFFSET + 23 * 2 + 1;
			break;
			case 'F':
				disp_tile[i] = OFFSET + 24 * 2;
				disp_tile[i+j] = OFFSET + 24 * 2 + 1;
			break;
			case 'S':
				disp_tile[i] = OFFSET + ('5' - '0') * 2;
				disp_tile[i+j] = OFFSET + ('5' - '0') * 2 + 1;
			break;
			case 'N':
				disp_tile[i] = OFFSET + 25 * 2;
				disp_tile[i+j] = OFFSET + 25 * 2 + 1;
			break;
			case 'E':
				disp_tile[i] = OFFSET + 26 * 2;
				disp_tile[i+j] = OFFSET + 26 * 2 + 1;
			break;
			case 'R':
				disp_tile[i] = OFFSET + 27 * 2;
				disp_tile[i+j] = OFFSET + 27 * 2 + 1;
			break;
			case 'P':
				disp_tile[i] = OFFSET + 28 * 2;
				disp_tile[i+j] = OFFSET + 28 * 2 + 1;
			break;
			case 'H':
				disp_tile[i] = OFFSET + 29 * 2;
				disp_tile[i+j] = OFFSET + 29 * 2 + 1;
			break;
			case 'K':
				disp_tile[i] = OFFSET + 30 * 2;
				disp_tile[i+j] = OFFSET + 30 * 2 + 1;
			break;
			case '_':
				disp_tile[i] = OFFSET + 31 * 2;
				disp_tile[i+j] = OFFSET + 31 * 2 + 1;
			break;
			case 'I':
				disp_tile[i] = OFFSET + 32 * 2;
				disp_tile[i+j] = OFFSET + 32 * 2 + 1;
			break;
		}
		i++;
	}
}

/* clear display */
void clr_disp()
{
	set_bkg_data(OFFSET, 66, dtmf_lcd);
	set_bkg_tiles(LCD_X, LCD_Y, LCD_WIDTH, LCD_HIGHT, init_disp);
}

/*
	CAUTION: Don't display the NULL code
*/
void disp(char str[MAX_DTMF])
{
	UBYTE no, left_pos;
	UBYTE i, start_ch, end_ch;
	char work[MAX_DTMF];

	clr_disp();

	no = 0;
	while(str[no]){
		no++;
	}

	if(no == NULL)
		return;

	if(no >= LCD_WIDTH){
		start_ch = no - LCD_WIDTH;
		end_ch = LCD_WIDTH;
	}
	else{
		start_ch = 0;
		end_ch = no;
	}
	for(i = 0;i < end_ch;i++){
		work[i] = str[i+start_ch];
	}
	work[end_ch] = 0x00;

	disp_lcd(end_ch, work);

	left_pos = 19 - end_ch;
	set_bkg_tiles(left_pos, LCD_Y, end_ch, LCD_HIGHT, disp_tile);
}

/*
	3 button size
	1 x 1
	1 x 2
	1 x 4
*/
void press_button(UBYTE x, UBYTE y, UBYTE area)
{
	switch (area){
		case 0:
		  set_bkg_tiles(x, y, 2, 2, press_tile_1x1);
		  break;
		case 1:
		  set_bkg_tiles(x, y, 4, 2, press_tile_2x1);
		  break;
		case 2:
		  set_bkg_tiles(x, y, 8, 2, press_tile_4x1);
		  break;
	}
}

void break_button(UBYTE x, UBYTE y, UBYTE area)
{
	switch (area){
		case 0:
		  set_bkg_tiles(x, y, 2, 2, break_tile_1x1);
		  break;
		case 1:
		  set_bkg_tiles(x, y, 4, 2, break_tile_2x1);
		  break;
		case 2:
		  set_bkg_tiles(x, y, 8, 2, break_tile_4x1);
		  break;
	}
}


void init_key()
{
	UBYTE key_x, key_y, i, j;

	/* To make numeric KeyPad */
	set_sprite_data(0, CURSOR_OBJ, key_num);

	/* key pad 1 - 3 */
	key_y = KEY_STEP + KEY_OFFSET_Y;
	for(i = 1;i <= 3;i++){
		key_x = i * KEY_STEP + KEY_OFFSET_X;
		set_sprite_tile(i, i);
		move_sprite(i, key_x, key_y);
	}

	/* key pad 4 - 6 */
	key_y = KEY_STEP * 2 + KEY_OFFSET_Y;
	for(i = 4;i <= 6;i++){
		key_x = (i - 3) * KEY_STEP + KEY_OFFSET_X;
		set_sprite_tile(i, i);
		move_sprite(i, key_x, key_y);
	}

	/* key pad 7 - 9 */
	key_y = KEY_STEP * 3 + KEY_OFFSET_Y;
		for(i = 7;i <= 9;i++){
			key_x = (i - 6) * KEY_STEP + KEY_OFFSET_X;
			set_sprite_tile(i, i);
			move_sprite(i, key_x, key_y);
		}

	/* key pad 'A' - 'D' */
	key_x = KEY_STEP * 4 + KEY_OFFSET_X;
	for(i = 0;i <= 3;i++){
		key_y = (i+1) * KEY_STEP + KEY_OFFSET_Y;
		set_sprite_tile(i+10, i+10);
		move_sprite(i+10, key_x, key_y);
	}

	/* key pad '*', '0', '#' */
	set_sprite_tile(15, 15);
	move_sprite(15, KEY_STEP * 1 + KEY_OFFSET_X, KEY_STEP * 4 + KEY_OFFSET_Y);
	set_sprite_tile(0, 0);
	move_sprite( 0, KEY_STEP * 2 + KEY_OFFSET_X, KEY_STEP * 4 + KEY_OFFSET_Y);
	set_sprite_tile(14, 14);
	move_sprite(14, KEY_STEP * 3 + KEY_OFFSET_X, KEY_STEP * 4 + KEY_OFFSET_Y);

	/* func AC */
	set_sprite_tile(16, 16);
	move_sprite(16, KEY_STEP * 4 + KEY_OFFSET_X, KEY_STEP * 5 + KEY_OFFSET_Y);
	
	/* func BS */
	set_sprite_tile(17, 17);
	move_sprite(17, KEY_STEP * 3 + KEY_OFFSET_X, KEY_STEP * 5 + KEY_OFFSET_Y);
	
	/* func ',' */
	set_sprite_tile(18, 18);
	move_sprite(18, KEY_STEP * 1 + KEY_OFFSET_X, KEY_STEP * 6 + KEY_OFFSET_Y);

	/* func ? */
	set_sprite_tile(19, 19);
	move_sprite(19, KEY_STEP * 2 + KEY_OFFSET_X, KEY_STEP * 6 + KEY_OFFSET_Y);

	/* func MODE */

	/* dialing button */
	set_sprite_tile(20, 20);
	move_sprite(20, KEY_STEP + KEY_STEP / 2 + KEY_OFFSET_X, KEY_STEP * 5 + KEY_OFFSET_Y);

	/* func Config. */
	set_sprite_tile(21, 21);
	move_sprite(21, KEY_STEP * 3 + KEY_OFFSET_X, KEY_STEP * 6 + KEY_OFFSET_Y);

	/* CCITT 2 6 0 0 */
	for(i = 0;i < 2;i++){
		set_sprite_tile(22 + i, 22 + i);
		move_sprite(22 + i, KEY_STEP / 2 * (i + 12) + KEY_OFFSET_X + 4, KEY_STEP * 1 + KEY_OFFSET_Y);
	}
	
	/* CCITT KP */
	set_sprite_tile(24, 24);
	move_sprite(24, KEY_STEP * 7 + KEY_OFFSET_X - 8, KEY_STEP * 2 + KEY_OFFSET_Y);

	
	/* CCITT ST */
	set_sprite_tile(25, 25);
	move_sprite(25, KEY_STEP * 7 + KEY_OFFSET_X - 8, KEY_STEP * 3 + KEY_OFFSET_Y);
	
	
	/* CCITT STp */
	for(i = 0;i < 2;i++){
		set_sprite_tile(26 + i, 26 + i);
		move_sprite(26 + i, KEY_STEP / 2 * (i + 11) + KEY_OFFSET_X - 4, KEY_STEP * 4 + KEY_OFFSET_Y);
	}
	
	/* CCITT ST2p */
	set_sprite_tile(28, 28);
	move_sprite(28, KEY_STEP / 2 * 11 + KEY_OFFSET_X, KEY_STEP * 5 + KEY_OFFSET_Y);
	
	
	/* CCITT ST3p */
	set_sprite_tile(29, 29);
	move_sprite(29, KEY_STEP / 2 * 11 + KEY_OFFSET_X, KEY_STEP * 6 + KEY_OFFSET_Y);
	
	
	/* CCITT 5cent */
	for(i = 0;i < 2;i++){
		set_sprite_tile(30 + i, 30 + i);
		move_sprite(30 + i, KEY_STEP / 2 * (i + 15) + KEY_OFFSET_X - 4, KEY_STEP * 4 + KEY_OFFSET_Y);
	}
	
	/* CCITT 10cent */
	for(i = 0;i < 2;i++){
		set_sprite_tile(32 + i, 32 + i);
		move_sprite(32 + i, KEY_STEP / 2 * (i + 15) + KEY_OFFSET_X - 4, KEY_STEP * 5 + KEY_OFFSET_Y);
	}
	
	/* CCITT 25cent */
	for(i = 0;i < 2;i++){
		set_sprite_tile(34 + i, 34 + i);
		move_sprite(34 + i, KEY_STEP / 2 * (i + 15) + KEY_OFFSET_X - 4, KEY_STEP * 6 + KEY_OFFSET_Y);
	}

}

void init_bkg()
{
	/* Initialize the background */
	set_bkg_data( 0, 9, frame_lcd);
	set_bkg_data( 9, 9, break_btn);
	set_bkg_data(18, 9, press_btn);
	
	set_bkg_tiles(0, 0, 20, 18, dtmf_tile);
}

void init_cursor()
{
	UBYTE i;

	/* Setup the cursor data*/
	set_sprite_data(CURSOR_OBJ, CURSOR_OBJ + CURSOR_SIZE, cursor_data);

	for(i = CURSOR_OBJ;i < CURSOR_OBJ + CURSOR_SIZE;i++){
		set_sprite_tile(i, i);
	}
}

void move_cursor(UBYTE x, UBYTE y)
{
	move_sprite(CURSOR_OBJ, x, y);
	move_sprite(CURSOR_OBJ + 1, x, y+8);
	move_sprite(CURSOR_OBJ + 2, x+8, y);
	move_sprite(CURSOR_OBJ + 3, x+8, y+8);
}


UWORD str02num(char str[CONFIG_PARM])
{
	return (str[0] - '0') * 10 + str[1] - '0';
}


void config_ph(char ch)
{
	char work[30];

	switch(config_st){
		/* on time setting */
		case 0:
			on_str[config_st % 2] = ch;
			strcpy(work, CON_MSG2);
			strcat(work, on_str);
			disp(work);
		break;
		case 1:
			on_str[config_st % 2] = ch;
			strcpy(work, CON_MSG2);
			strcat(work, on_str);
			disp(work);
			on_time = str02num(on_str);
			delay(500);

			/* For the next step */
			strcpy(work, CON_MSG3);
			strcat(work, off_str);
			disp(work);
		break;

		/* off_time setting */
		case 2:
			off_str[config_st % 2] = ch;
			strcpy(work, CON_MSG3);
			strcat(work, off_str);
			disp(work);
		break;
		case 3:
			off_str[config_st % 2] = ch;
			strcpy(work, CON_MSG3);
			strcat(work, off_str);
			disp(work);
			off_time = str02num(off_str);
			delay(500);

			/* For the next step */
			strcpy(work, CON_MSG4);
			strcat(work, rpt_str);
			disp(work);
		break;

		/* repeat setting */
		case 4:
			rpt_str[config_st % 2] = ch;
			strcpy(work, CON_MSG4);
			strcat(work, rpt_str);
			disp(work);
		break;
		case 5:
			rpt_str[config_st % 2] = ch;
			strcpy(work, CON_MSG4);
			strcat(work, rpt_str);
			disp(work);
			rpt_time = str02num(rpt_str);
			delay(500);

			/* For the next step */
			strcpy(work, CON_MSG5);
			strcat(work, pause_str);
			disp(work);
		break;

		/* pause setting */
		case 6:
			pause_str[config_st % 2] = ch;
			strcpy(work, CON_MSG5);
			strcat(work, pause_str);
			disp(work);
		break;
		case 7:
			pause_str[config_st % 2] = ch;
			strcpy(work, CON_MSG5);
			strcat(work, pause_str);
			disp(work);
			pause_time = str02num(pause_str);
			delay(500);
		break;

	}
	config_st++;
	if(config_st >= CONFIG_LIMIT){
		config_st = NORMAL;
		mode_excute = NORMAL;
		disp(CON_MSG6);
		delay(1000);
		disp(str);
	}
}
void parm_init()
{
	/* default dialling time setting */
	on_time = DTMF_ON;
	strcpy(on_str, "50");
	
	off_time = DTMF_OFF;
	strcpy(off_str, "50");
	
	rpt_time = 1;
	strcpy(rpt_str, "01");
	
	pause_time = 99;
	strcpy(pause_str, "99");

	config_st = NORMAL;
	mode_excute = NORMAL;
	disp_mode = DISP_DTMF;
}

void main()
{
	UBYTE key1, key2, i, j, pos_x, pos_y, ch_pos, button_x, button_y;
	UBYTE non_flick = OFF;

	char work[20];
	char pad_key;

	parm_init();

	disable_interrupts();
	
	SPRITES_8x8;   /* sprites are 8x8 */

	init_dial();
	
	init_bkg();
	
	init_key();

	init_cursor();

	disp(TITLE);

	SHOW_BKG;
	SHOW_SPRITES;
	DISPLAY_ON;

	enable_interrupts();
	
	i = j = 0;

	ch_pos = 0;
	
	while(1) {
		delay(10);
		key1 = joypad();
		pad_key = pad[j][i];
		button_x = i * 2;
		button_y = j * 2 + 5;

		if(key1 != key2){
			pos_x = i * KEY_STEP + START_CURSOR_X;
			pos_y = j * KEY_STEP + START_CURSOR_Y;
			move_cursor(pos_x, pos_y);
		}

	/*
		KEY status
		
		-----------+            +-----------
             ^     |            |   ^
             |     |            |   |
             |     |            |   |
             |     +------------+   |
             |     ^      ^     ^   |
             |     |      |     |   +-------
             |     |      |     +-----------
             |     |      +-----------------
             |     +------------------------
             +------------------------------
             
    */
		if(key2 & J_A){
			if(key1 & J_A){
				switch (pad_key){
				/* We can not make matrix pattern at CCITT 5 as like as DTMF */
				/* Also we have to define a each digit with two combination Fx */
					case '0':
						if(!disp_mode)
					  		start_sound(row_dtmf[j],col_dtmf[i - 2]);
					  	else
							start_sound(F4, F5);
						break;
					case '1':
						if(!disp_mode)
							start_sound(row_dtmf[j],col_dtmf[i - 2]);
						else
							start_sound(F1, F2);
						break;
					case '2':
						if(!disp_mode)
							start_sound(row_dtmf[j],col_dtmf[i - 2]);
						else
							start_sound(F1, F3);
						break;
					case '3':
						if(!disp_mode)
							start_sound(row_dtmf[j],col_dtmf[i - 2]);
						else
							start_sound(F2, F3);
						break;
					case '4':
						if(!disp_mode)
							start_sound(row_dtmf[j],col_dtmf[i - 2]);
						else
							start_sound(F1, F4);
						break;
					case '5':
						if(!disp_mode)
							start_sound(row_dtmf[j],col_dtmf[i - 2]);
						else
							start_sound(F2, F4);
						break;
					case '6':
						if(!disp_mode)
							start_sound(row_dtmf[j],col_dtmf[i - 2]);
						else
							start_sound(F3, F4);
						break;
					case '7':
						if(!disp_mode)
							start_sound(row_dtmf[j],col_dtmf[i - 2]);
						else
							start_sound(F1, F5);
						break;
					case '8':
						if(!disp_mode)
							start_sound(row_dtmf[j],col_dtmf[i - 2]);
						else
							start_sound(F2, F5);
						break;
					case '9':
						if(!disp_mode)
							start_sound(row_dtmf[j],col_dtmf[i - 2]);
						else
							start_sound(F3, F5);
						break;
					case 'A':
					case 'B':
					case 'C':
					case 'D':
					case '#':
					case '*':
						if(!disp_mode)
							start_sound(row_dtmf[j],col_dtmf[i - 2]);
					break;
					case '?':
						/* appear the title during press A button */
						if(!mode_excute){
							if(!non_flick){
								if(!disp_mode)
									disp(DTMF);
								else
									disp(CCITT);
								delay(1000);
								strcpy(work, "  ");
								strcat(work, on_str);
								
								strcat(work, "  ");
								strcat(work, off_str);
								
								strcat(work, "  ");
								strcat(work, rpt_str);
								
								strcat(work, "  ");
								strcat(work, pause_str);
								
								strcat(work, "  ");
								disp(work);
								non_flick = ON;
							}
						}
					break;
					case 'a':
					  /* frequncy register set up for CCITT 5 */
					  NR13_REG = F9;
					  NR23_REG = F9;
					  NR24_REG = 0x87U;

					  /* sound output on */
					  NR51_REG = 0x33U;
					break;
				}
			}
			
			/* Button is just re-leased */
			/* Make -> Break */
			else{
				/* sound output off */
				NR51_REG = 0x00U;
				switch (pad_key){
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
					case 'A':
					case 'B':
					case 'C':
					case 'D':
					case '#':
					case '*':
					case '-':
					case '%':
					case ',':
					  break_button(button_x, button_y, 0);
					break;

					case '?':
						break_button(button_x, button_y, 0);
						if(!mode_excute){
							non_flick = OFF;
							if(ch_pos == 0)
								clr_disp();
							else
								disp(str);
						}
					break;

					case 'm':
						break_button(button_x, button_y, 0);
					break;

					case '+':
						break_button(button_x, button_y, 0);
					break;

					case 's':
					  break_button(4, 13, 1);
					break;
					
					/* 2600 button */
					case 'a':
						break_button(12, 5, 2);
					break;
					
					/* Kp button */
					case 'b':
						break_button(12, 7, 2);
					break;
					
					/* ST button */
					case 'c':
						break_button(12, 9, 2);
					break;
					
					/* STp button */
					case 'd':
						break_button(12, 11, 1);
					break;
					
					/* ST2p button */
					case 'e':
						break_button(12, 13, 1);
					break;
					
					/* ST3p button */
					case 'f':
						break_button(12, 15, 1);
					break;
					
					/* 5cent button */
					case 'g':
						break_button(16, 11, 1);
					break;
					
					/* 10cent button */
					case 'h':
						break_button(16, 13, 1);
					break;
					
					/* 25cent button */
					case 'i':
						break_button(16, 15, 1);
					break;
					
					/* Function key setting */
					/* w */
					case 'w':
						break_button(0, 11, 1);
					break;

					/* v */
					case 'v':
						break_button(0, 13, 1);
					break;

					/* u */
					case 'u':
						break_button(0, 15, 1);
					break;
					
				}
			}
		}
		else{
			/* Just pressed */
			/* Break --> Make */
			if(key1 & J_A){
				switch (pad_key){
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
						press_button(button_x, button_y, 0);
						if(!mode_excute){
							/* string length check */
							if(ch_pos < MAX_DTMF - 1){
								str[ch_pos] = pad[j][i];
								ch_pos++;
								str[ch_pos] = 0x00;
								disp(str);
							}
						}
						else
							config_ph(pad[j][i]);
					break;
					case 'A':
					case 'B':
					case 'C':
					case 'D':
					case '#':
					case '*':
					case ',':
						press_button(button_x, button_y, 0);
						if(!mode_excute){
							/* string length check */
							if(ch_pos < MAX_DTMF - 1){
								str[ch_pos] = pad[j][i];
								ch_pos++;
								str[ch_pos] = 0x00;
								disp(str);
							}
						}
					break;

					/* all clear button */
					case '%':
						press_button(button_x, button_y, 0);
						if(!mode_excute){
							ch_pos = 0x00;
							strcpy(str,"");
							clr_disp();
						}
					break;

					/* BS button */
					case '-':
						press_button(button_x, button_y, 0);
						if(!mode_excute){
							if(ch_pos > 0){
								ch_pos--;
								str[ch_pos] = 0x00;
								if(ch_pos == 0)
									clr_disp();
								else
									disp(str);
							}
						}
						else{
						}
					break;

					/* ? button */
					case '?':
						press_button(button_x, button_y, 0);
					break;

					/* MODE button */
					case 'm':
						press_button(button_x, button_y, 0);
						disp_mode = !disp_mode;
						if(disp_mode)
							disp(CCITT);
						else
							disp(DTMF);
						delay(1000);
						disp(str);
					break;

					/* Config button */
					case '+':
						press_button(button_x, button_y, 0);
						if(!mode_excute){
							disp(CON_MSG1);
							delay(1000);
							mode_excute = CONFIG;
							config_st = NORMAL;
							/* initialize variable */
							strcpy(on_str, "--");
							strcpy(off_str, "--");
							strcpy(rpt_str, "--");
							strcpy(pause_str, "--");
							strcpy(work, "");

							/* For the on time */
							strcat(work, CON_MSG2);
							strcat(work, on_str);
							disp(work);
						}
					break;

				 	/* dialing button */
					case 's':
						press_button(4, 13, 1);
						if(!disp_mode)
							dialtone(on_time, off_time, str);
						else
							ccitttone(on_time, off_time, str);
					break;
					
					/* 2600 button */
					case 'a':
						press_button(12, 5, 2);
					break;
					
					/* Kp button */
					case 'b':
						press_button(12, 7, 2);
						start_sound(F3, F6);
						pause_sound(SOUND_ON, SOUND_ON);
					break;
					
					/* ST button */
					case 'c':
						press_button(12, 9, 2);
						start_sound(F5, F6);
						pause_sound(SOUND_ON, SOUND_ON);
					break;
					
					/* STp button */
					case 'd':
						press_button(12, 11, 1);
						start_sound(F2, F6);
						pause_sound(SOUND_ON, SOUND_ON);
					break;
					
					/* ST2p button */
					case 'e':
						press_button(12, 13, 1);
						start_sound(F4, F6);
						pause_sound(SOUND_ON, SOUND_ON);
					break;
					
					/* ST3p button */
					case 'f':
						press_button(12, 15, 1);
						start_sound(F1, F6);
						pause_sound(SOUND_ON, SOUND_ON);
					break;

					/* 5cent button */
					case 'g':
						press_button(16, 11, 1);
						coin_sound(NICKLE);
					break;

					/* 10cent button */
					case 'h':
						press_button(16, 13, 1);
						coin_sound(DIME);
					break;

					/* 25cent button */
					case 'i':
						press_button(16, 15, 1);
						coin_sound(QUARTER);
					break;

					/* Function key setting */

					/* w */
					/* fastest dialing setting */
					case 'w':
						press_button(0, 11, 1);
						parm_init();
						on_time = 25;
						strcpy(on_str, "25");
						
						off_time = 35;
						strcpy(off_str, "35");
					break;

					/* v */
					/* Answering machine cracking */
					case 'v':
						press_button(0, 13, 1);
						parm_init();
						on_time = 20;
						strcpy(on_str, "20");
						
						off_time = 0;
						strcpy(off_str, "00");
						
						rpt_time = 4;
						strcpy(rpt_str, "04");
						
						pause_time = 99;
						strcpy(pause_str, "99");
						
						strcpy(str, "1234567890");
						ch_pos = 10;
						disp(str);
					break;

					/* u */
					/* default setting */
					case 'u':
						press_button(0, 15, 1);
						parm_init();
					break;

					
				}
			}
		}

		if(!(key1 & J_A)){
			if((key1 & J_UP) && !(key2 & J_UP) && j > 0)
				j--;
			else if((key1 & J_DOWN) && !(key2 & J_DOWN) && j < UNIT_SIZE_Y)
				j++;

			if((key1 & J_LEFT) && !(key2 & J_LEFT) && i > 0)
				i--;
			else if((key1 & J_RIGHT) && !(key2 & J_RIGHT) && i < UNIT_SIZE_X)
				i++;
		}
		key2 = key1;
	}
}
